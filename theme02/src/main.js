import Raphael from 'raphael/raphael'

global.Raphael = Raphael
import Vue from 'vue'
import App from './App.vue'
import Grid from 'gridjs-vue'
import { BTable } from 'bootstrap-vue'
import VueRouter from 'vue-router'
import VueSimpleAlert from "vue-simple-alert";

Vue.use(VueSimpleAlert);
Vue.use(VueRouter)

import 'bootstrap/dist/css/bootstrap.min.css'
import Homepage from "@/components/Homepage";
import Login from "@/components/User/Login";
import Signup from "@/components/User/Signup";
import Edit from "@/components/User/Edit";
import Workingtimes from "@/components/Workingtimes/Workingtimes";
import Workingtime from "@/components/Workingtime/Workingtime";
import Clocking from "@/components/Clocking/Clocking";
import NotFound from "@/components/NotFound";
import Dashboard from "@/components/Dashboard";
import Team from "@/components/Team/Team";
import Teams from "@/components/Team/Teams";
import Users from "@/components/Users/Users";

Vue.config.productionTip = false
Vue.component('b-table', BTable)

const routes = [
    {path: '/', component: Homepage},
    {path: '/login', component: Login},
    {path: '/edit', component: Edit},
    {path: '/signup', component: Signup},
    {path: '/workingtimes', component: Workingtimes},
    {path: '/workingtime', component: Workingtime},
    {path: '/clocking', component: Clocking},
    {path: '/team', component: Team},
    {path: '/teams', component: Teams},
    {path: '/dashboard', component: Dashboard},
    {path: '/users', component: Users},
    {path: '*', component: NotFound}
]

const router = new VueRouter({
    mode: 'history',
    routes: routes
})
new Vue({
    router,
    render: h => h(App),
}).$mount('#app')
Vue.use(Grid)
