# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     AppTest.Repo.insert!(%AppTest.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias AppTest.Repo
alias AppTest.Data.Users

#General Manager
AppTest.Data.create_users(%{username: "Alexandre Garcia Accar", email: "alexandre.garcia-accar@epitech.eu", password: "123456789", role: "General Manager"})
AppTest.Data.create_users(%{username: "François de Taxis du Poët", email: "francoisdetaxis@gmail.com", password: "123456789", role: "General Manager"})
AppTest.Data.create_users(%{username: "Oren Cohen", email: "oren.cohen@epitech.eu", password: "123456789", role: "General Manager"})
AppTest.Data.create_users(%{username: "Dan Cohen", email: "dan.cohen@epitech.eu", password: "123456789", role: "General Manager"})

#Managers
AppTest.Data.create_users(%{username: "Tammy Talley", email: "tammy.talley@gmail.com", password: "123456789", role: "Manager"})
AppTest.Data.create_users(%{username: "Alfie-James Mooney", email: "alfie-james.mooney@gmail.com", password: "123456789", role: "Manager"})

#Employees
AppTest.Data.create_users(%{username: "Rumaisa Castillo", email: "rumaisa.castillo@gmail.com", password: "123456789", role: "Employee"})
AppTest.Data.create_users(%{username: "Abu Bourne", email: "abu.bourne@gmail.com", password: "123456789", role: "Employee"})
AppTest.Data.create_users(%{username: "Hal Goulding", email: "hal.goulding@gmail.com", password: "123456789", role: "Employee"})
AppTest.Data.create_users(%{username: "Marjorie Ramirez", email: "marjorie.ramirez@yahoo.fr", password: "123456789", role: "Employee"})
AppTest.Data.create_users(%{username: "Arnie Chadwick", email: "arnie.chadwick@gmail.com", password: "123456789", role: "Employee"})
AppTest.Data.create_users(%{username: "Miyah Walsh", email: "miyah.walsh@gmail.com", password: "123456789", role: "Employee"})
AppTest.Data.create_users(%{username: "Mary Schofield", email: "mary.schofield@gmail.com", password: "123456789", role: "Employee"})
AppTest.Data.create_users(%{username: "Eloise Woolley", email: "eloise.woolley@gmail.com", password: "123456789", role: "Employee"})
AppTest.Data.create_users(%{username: "Ahsan Lin", email: "ahsan.lin@gmail.com", password: "123456789", role: "Employee"})
AppTest.Data.create_users(%{username: "Amelia Stanton", email: "amelia.stanton@gmail.com", password: "123456789", role: "Employee"})
AppTest.Data.create_users(%{username: "Huseyin Connor", email: "huseyin.connor@gmail.com", password: "123456789", role: "Employee"})


########################################################################################################################
#                                  workingtimes for userID = 10 // Marjorie Ramirez
########################################################################################################################
user = Repo.get(Users, 10)

startDate = NaiveDateTime.from_iso8601!("2020-10-17 08:37:48")
endDate = NaiveDateTime.from_iso8601!("2020-10-17 12:37:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

  startDate= NaiveDateTime.from_iso8601!("2020-10-17 13:37:48")
endDate = NaiveDateTime.from_iso8601!("2020-10-17 18:37:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2020-10-18 08:42:48")
endDate = NaiveDateTime.from_iso8601!("2020-10-18 12:20:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2020-10-18 13:56:48")
endDate = NaiveDateTime.from_iso8601!("2020-10-18 18:12:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2020-10-19 08:42:48")
endDate = NaiveDateTime.from_iso8601!("2020-10-19 12:20:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2020-10-19 13:56:48")
endDate = NaiveDateTime.from_iso8601!("2020-10-19 18:12:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2021-10-17 08:37:48")
endDate = NaiveDateTime.from_iso8601!("2021-10-17 12:37:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate= NaiveDateTime.from_iso8601!("2021-10-17 13:37:48")
endDate = NaiveDateTime.from_iso8601!("2021-10-17 18:37:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2021-10-18 08:42:48")
endDate = NaiveDateTime.from_iso8601!("2021-10-18 12:20:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2021-10-18 13:56:48")
endDate = NaiveDateTime.from_iso8601!("2021-10-18 18:12:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2021-10-19 08:42:48")
endDate = NaiveDateTime.from_iso8601!("2021-10-19 12:20:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2021-10-19 13:56:48")
endDate = NaiveDateTime.from_iso8601!("2021-10-19 18:12:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2022-10-17 08:37:48")
endDate = NaiveDateTime.from_iso8601!("2022-10-17 12:37:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate= NaiveDateTime.from_iso8601!("2022-10-17 13:37:48")
endDate = NaiveDateTime.from_iso8601!("2022-10-17 18:37:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2022-10-18 08:42:48")
endDate = NaiveDateTime.from_iso8601!("2022-10-18 12:20:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2022-10-18 13:56:48")
endDate = NaiveDateTime.from_iso8601!("2022-10-18 18:12:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2022-10-19 08:42:48")
endDate = NaiveDateTime.from_iso8601!("2022-10-19 12:20:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2022-10-19 13:56:48")
endDate = NaiveDateTime.from_iso8601!("2022-10-19 18:12:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2023-10-17 08:37:48")
endDate = NaiveDateTime.from_iso8601!("2023-10-17 12:37:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate= NaiveDateTime.from_iso8601!("2023-10-17 13:37:48")
endDate = NaiveDateTime.from_iso8601!("2023-10-17 18:37:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2023-10-18 08:42:48")
endDate = NaiveDateTime.from_iso8601!("2023-10-18 12:20:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2023-10-18 13:56:48")
endDate = NaiveDateTime.from_iso8601!("2023-10-18 18:12:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2023-10-19 08:42:48")
endDate = NaiveDateTime.from_iso8601!("2023-10-19 12:20:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2023-10-19 13:56:48")
endDate = NaiveDateTime.from_iso8601!("2023-10-19 18:12:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2024-10-17 08:37:48")
endDate = NaiveDateTime.from_iso8601!("2024-10-17 12:37:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate= NaiveDateTime.from_iso8601!("2024-10-17 13:37:48")
endDate = NaiveDateTime.from_iso8601!("2024-10-17 18:37:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2024-10-18 08:42:48")
endDate = NaiveDateTime.from_iso8601!("2024-10-18 12:20:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2024-10-18 13:56:48")
endDate = NaiveDateTime.from_iso8601!("2024-10-18 18:12:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2024-10-19 08:42:48")
endDate = NaiveDateTime.from_iso8601!("2024-10-19 12:20:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2024-10-19 13:56:48")
endDate = NaiveDateTime.from_iso8601!("2024-10-19 18:12:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2025-10-17 08:37:48")
endDate = NaiveDateTime.from_iso8601!("2025-10-17 12:37:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate= NaiveDateTime.from_iso8601!("2025-10-17 13:37:48")
endDate = NaiveDateTime.from_iso8601!("2025-10-17 18:37:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2025-10-18 08:42:48")
endDate = NaiveDateTime.from_iso8601!("2025-10-18 12:20:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2025-10-18 13:56:48")
endDate = NaiveDateTime.from_iso8601!("2025-10-18 18:12:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2025-10-19 08:42:48")
endDate = NaiveDateTime.from_iso8601!("2025-10-19 12:20:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2025-10-19 13:56:48")
endDate = NaiveDateTime.from_iso8601!("2025-10-19 18:12:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)
########################################################################################################################
#                                  workingtimes for userID = 11 // Arnie Chadwick
########################################################################################################################
user = Repo.get(Users, 11)

startDate = NaiveDateTime.from_iso8601!("2020-10-17 08:37:48")
endDate = NaiveDateTime.from_iso8601!("2020-10-17 12:37:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate= NaiveDateTime.from_iso8601!("2020-10-17 13:37:48")
endDate = NaiveDateTime.from_iso8601!("2020-10-17 18:37:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2020-10-18 08:42:48")
endDate = NaiveDateTime.from_iso8601!("2020-10-18 12:20:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2020-10-18 13:56:48")
endDate = NaiveDateTime.from_iso8601!("2020-10-18 18:12:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2020-10-19 08:42:48")
endDate = NaiveDateTime.from_iso8601!("2020-10-19 12:20:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2020-10-19 13:56:48")
endDate = NaiveDateTime.from_iso8601!("2020-10-19 18:12:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2021-10-17 08:37:48")
endDate = NaiveDateTime.from_iso8601!("2021-10-17 12:37:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate= NaiveDateTime.from_iso8601!("2021-10-17 13:37:48")
endDate = NaiveDateTime.from_iso8601!("2021-10-17 18:37:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2021-10-18 08:42:48")
endDate = NaiveDateTime.from_iso8601!("2021-10-18 12:20:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2021-10-18 13:56:48")
endDate = NaiveDateTime.from_iso8601!("2021-10-18 18:12:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2021-10-19 08:42:48")
endDate = NaiveDateTime.from_iso8601!("2021-10-19 12:20:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2021-10-19 13:56:48")
endDate = NaiveDateTime.from_iso8601!("2021-10-19 18:12:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2022-10-17 08:37:48")
endDate = NaiveDateTime.from_iso8601!("2022-10-17 12:37:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate= NaiveDateTime.from_iso8601!("2022-10-17 13:37:48")
endDate = NaiveDateTime.from_iso8601!("2022-10-17 18:37:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2022-10-18 08:42:48")
endDate = NaiveDateTime.from_iso8601!("2022-10-18 12:20:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2022-10-18 13:56:48")
endDate = NaiveDateTime.from_iso8601!("2022-10-18 18:12:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2022-10-19 08:42:48")
endDate = NaiveDateTime.from_iso8601!("2022-10-19 12:20:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2022-10-19 13:56:48")
endDate = NaiveDateTime.from_iso8601!("2022-10-19 18:12:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2023-10-17 08:37:48")
endDate = NaiveDateTime.from_iso8601!("2023-10-17 12:37:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate= NaiveDateTime.from_iso8601!("2023-10-17 13:37:48")
endDate = NaiveDateTime.from_iso8601!("2023-10-17 18:37:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2023-10-18 08:42:48")
endDate = NaiveDateTime.from_iso8601!("2023-10-18 12:20:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2023-10-18 13:56:48")
endDate = NaiveDateTime.from_iso8601!("2023-10-18 18:12:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2023-10-19 08:42:48")
endDate = NaiveDateTime.from_iso8601!("2023-10-19 12:20:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2023-10-19 13:56:48")
endDate = NaiveDateTime.from_iso8601!("2023-10-19 18:12:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2024-10-17 08:37:48")
endDate = NaiveDateTime.from_iso8601!("2024-10-17 12:37:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate= NaiveDateTime.from_iso8601!("2024-10-17 13:37:48")
endDate = NaiveDateTime.from_iso8601!("2024-10-17 18:37:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2024-10-18 08:42:48")
endDate = NaiveDateTime.from_iso8601!("2024-10-18 12:20:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2024-10-18 13:56:48")
endDate = NaiveDateTime.from_iso8601!("2024-10-18 18:12:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2024-10-19 08:42:48")
endDate = NaiveDateTime.from_iso8601!("2024-10-19 12:20:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2024-10-19 13:56:48")
endDate = NaiveDateTime.from_iso8601!("2024-10-19 18:12:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2025-10-17 08:37:48")
endDate = NaiveDateTime.from_iso8601!("2025-10-17 12:37:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate= NaiveDateTime.from_iso8601!("2025-10-17 13:37:48")
endDate = NaiveDateTime.from_iso8601!("2025-10-17 18:37:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2025-10-18 08:42:48")
endDate = NaiveDateTime.from_iso8601!("2025-10-18 12:20:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2025-10-18 13:56:48")
endDate = NaiveDateTime.from_iso8601!("2025-10-18 18:12:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2025-10-19 08:42:48")
endDate = NaiveDateTime.from_iso8601!("2025-10-19 12:20:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)

startDate = NaiveDateTime.from_iso8601!("2025-10-19 13:56:48")
endDate = NaiveDateTime.from_iso8601!("2025-10-19 18:12:48")
working_time_changeset = Ecto.build_assoc(user, :workingtimes, %{end: endDate, start: startDate})
Repo.insert!(working_time_changeset)

clock_changeset = Ecto.build_assoc(user, :clocks, %{time: startDate, status: true})
Repo.insert!(clock_changeset)
clock_changeset = Ecto.build_assoc(user, :clocks, %{time: endDate, status: false})
Repo.insert!(clock_changeset)