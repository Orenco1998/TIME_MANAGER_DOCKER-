defmodule AppTest.Guardian.AuthPipeline do
  use Guardian.Plug.Pipeline, otp_app: :AppTest,
                              module: AppTest.Guardian,
                              error_handler: AppTest.AuthErrorHandler

  plug Guardian.Plug.VerifyHeader, realm: "Bearer"
  plug Guardian.Plug.EnsureAuthenticated
  plug Guardian.Plug.LoadResource
end
