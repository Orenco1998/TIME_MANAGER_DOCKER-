defmodule AppTestWeb.Router do
  use AppTestWeb, :router

  alias AppTest.Guardian

  #PIPELINES
  #PIPELINE FOR NON-AUTHENTICATED USERS
  pipeline :api do
    plug :accepts, ["json"]
    plug CORSPlug, origin: "*"
  end

  #PIPELINE FOR AUTHENTICATED USERS
  pipeline :jwt_authenticated do
    plug CORSPlug, origin: "*"
    plug Guardian.AuthPipeline
  end

  #ROUTES
  #AUTHENTICATED ROUTES
  scope "/api", AppTestWeb do
    pipe_through :jwt_authenticated
    #USERS
    get "/users/sign_out", UsersController, :sign_out
    get "/users", UsersController, :get_all_users
    options "/users", UsersController, :options
    resources "/users", UsersController, except: [:new, :edit]

    #WORKINGTIMES
    post("/workingtimes/:usersId", WorkingtimesController, :create)
    get("/workingtimes/", WorkingtimesController, :index)
    put("/workingtimes/:id", WorkingtimesController, :update)
    delete("/workingtimes/:id", WorkingtimesController, :delete)

    #CLOCKS
    post("/clocks/:usersId", ClocksController, :create)
    get("/clocks/:usersId", ClocksController, :show)
    delete("/clocks/:id", ClocksController, :delete)

    #TEAMS
    post("/teams", TeamsController, :create)
    get("/teams", TeamsController, :index)
    post("/teamsUsers", TeamsController, :addUsersInTeams)
    get("/teamsUsers", TeamsController, :getUsersInTeams)
    delete("/teamsUsers/:teamsId/:usersId", TeamsController, :deleteUsersInTeams)
    delete("/teams", TeamsController, :delete)
    put("/teams", TeamsController, :update)
#    get("/teamsUsers/:teamsId", TeamsController, :getTeamsInUsers)
    resources "/teams", TeamsController, except: [:new, :edit]

    #post "/workingtimes/:usersid", WorkingtimesController, :create
  end

  #NON-AUTHENTICATED ROUTES
  scope "/api", AppTestWeb do
    pipe_through :api
    #USERS
    post "/users/sign_up", UsersController, :sign_up
    post "/users/sign_in", UsersController, :sign_in
  end
end
