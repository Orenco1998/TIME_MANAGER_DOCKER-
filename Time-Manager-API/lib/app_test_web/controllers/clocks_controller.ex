defmodule AppTestWeb.ClocksController do
  use AppTestWeb, :controller

  alias AppTest.Data
  alias AppTest.Data.Clocks
  alias AppTest.Repo
  alias AppTest.Data.Users

  action_fallback AppTestWeb.FallbackController

  #  def index(conn, _params) do
  #
  #    clocks = Data.list_clocks()
  #    render(conn, "index.json", clocks: clocks)
  #    #        conn
  #    #        |> put_status(:unauthorized)
  #    #        |> render("401.json", message: message)
  #  end

  def create(conn, %{"clocks" => clocks_params, "usersId" => usersId}) do
    #you can only create clocks for yourself...
    #OR if you are the general manager
    jwt_users = Guardian.Plug.current_resource(conn)

    if (Integer.to_string(jwt_users.id) == usersId or jwt_users.role == "General Manager") do
      user = Repo.get(Users, usersId)
      timeclock = NaiveDateTime.from_iso8601!(clocks_params["time"])
      statusclock = clocks_params["status"]
      clock_changeset = Ecto.build_assoc(user, :clocks, %{time: timeclock, status: statusclock})
      {:ok, %Clocks{} = clockss} = Repo.insert(clock_changeset)
      conn
      |> put_status(:created)
      |> render("show.json", clocks: clockss)
    else
      conn
      |> put_status(:unauthorized)
      |> render("401.json", message: "You cannot create clockings for users other than yourself.")
    end
  end

  def show(conn, %{"usersId" => usersId}) do
    #any role can see the clocks of a particular usersID
    clockss = Data.get_clocks_by_user(usersId)
    render(conn, "index.json", clocks: clockss)
  end

  def delete(conn, %{"id" => id}) do
    #you can only delete clocks for yourself...
    #OR if you are the general manager
    jwt_users = Guardian.Plug.current_resource(conn)
    clock = Data.get_clocks!(id)

    if (jwt_users.id == clock.users_id or jwt_users.role == "General Manager") do
      with {:ok, %Clocks{}} <- Data.delete_clocks(clock) do
        send_resp(conn, :no_content, "")
      end
    else
      conn
      |> put_status(:unauthorized)
      |> render("401.json", message: "You cannot delete clockings for users other than yourself.")
    end
  end

end
