defmodule AppTestWeb.WorkingtimesController do
  use AppTestWeb, :controller

  alias AppTest.Data
  alias AppTest.Data.Workingtimes
  alias AppTest.Repo
  alias AppTest.Data.Users
  action_fallback AppTestWeb.FallbackController

  def index(conn, %{"id" => id, "type" => type}) do
    #TODO restrictions par rapport au role de l'utilisateur necessaire ?
    #id can be either one of a user or a workingtime
    jwt_users = Guardian.Plug.current_resource(conn)

    case type do
      "users" ->
        if (jwt_users.role == "General Manager" or Integer.to_string(jwt_users.id) == id) do
          working_times = Data.get_all_workingtimes_by_user(id)
          render(conn, "index.json", workingtimes: working_times)
        else
          conn
          |> put_status(:unauthorized)
          |> render("401.json", message: "You cannot access the working times of other users.")
        end
      "workingtimes" ->
        working_times = Data.get_workingtimes!(id)
        if (jwt_users.role == "General Manager" or jwt_users.id == working_times.users_id) do
          IO.puts "\nsomething fishy going on here\n"
          IO.inspect(jwt_users)
          IO.inspect(working_times)
          IO.puts "\nsomething fishy going on here\n"
          render(conn, "workingtimes.json", workingtimes: working_times)
        else
          conn
          |> put_status(:unauthorized)
          |> render("401.json", message: "You cannot access the working times of other users.")
        end
      _ ->
        conn
        |> put_status(:unauthorized)
        |> render("401.json", message: "Invalid type parameter. Valid types are : users, workingtimes.")
    end
  end

  def index(conn, params) do
    #TODO restrictions par rapport au role de l'utilisateur necessaire ?
    startDate = params["start"]
    endDate = params["end"]
    userID = params["usersId"]
    working_times = Data.get_all_workingtimes_by_user!(userID, startDate, endDate)
    render(conn, "index.json", workingtimes: working_times)
  end

  def create(conn, %{"workingtimes" => workingtimes_params, "usersId" => usersId}) do
    jwt_users = Guardian.Plug.current_resource(conn)
    if (Integer.to_string(jwt_users.id) == usersId) do
      endDate = NaiveDateTime.from_iso8601!(workingtimes_params["end"])
      startDate = NaiveDateTime.from_iso8601!(workingtimes_params["start"])
      working_time_changeset = Ecto.build_assoc(jwt_users, :workingtimes, %{end: endDate, start: startDate})
      result = Repo.insert!(working_time_changeset)
      case result do
        #TODO : this matches everything, handles errors....
        _ ->
          conn
          |> put_status(:created)
          |> render("show.json", workingtimes: result)
      end
    else
      conn
      |> put_status(:unauthorized)
      |> render("401.json", message: "You cannot create workingtimes for users other than yourself.")
    end
  end

  #def show(conn, %{"usersId" => usersId, "id" => id}) do
  # working_times = Data.get_one_workingtimes_by_user!(usersId, id)
  #render(conn, "show.json", workingtimes: working_times)
  #end

  #def show(conn, %{"id" => id}) do
  #workingtimes = Data.get_workingtimes!(id)
  # render(conn, "show.json", workingtimes: workingtimes)
  #end

  def update(conn, %{"id" => id, "workingtimes" => workingtimes_params}) do
    jwt_users = Guardian.Plug.current_resource(conn)
    workingtimes = Data.get_workingtimes!(id)
    if (workingtimes.users_id == jwt_users.id) do
      with {:ok, %Workingtimes{} = workingtimes} <- Data.update_workingtimes(workingtimes, workingtimes_params) do
        render(conn, "show.json", workingtimes: workingtimes)
      end
    else
      conn
      |> put_status(:unauthorized)
      |> render("401.json", message: "You cannot edit the workingtime of another user.")
    end
  end

  def delete(conn, %{"id" => id}) do
    #User can only delete his own workingtimes (except the General Manager, he can do everything)
    jwt_users = Guardian.Plug.current_resource(conn)
    workingtimes = Data.get_workingtimes!(id)

    if (jwt_users.role == "General Manager" or jwt_users.id == workingtimes.users_id) do
      with {:ok, %Workingtimes{}} <- Data.delete_workingtimes(workingtimes) do
        send_resp(conn, :no_content, "")
      end
    else
      conn
      |> put_status(:unauthorized)
      |> render("401.json", message: "You cannot delete workingtimes for users other than yourself.")
    end

  end

end
