defmodule AppTestWeb.TeamsController do
  use AppTestWeb, :controller

  alias AppTest.Data
  alias AppTest.Data.Teams
  alias AppTest.Data.UsersTeams

  action_fallback AppTestWeb.FallbackController

  def index(conn, _params) do
    teams = Data.list_teams()
    render(conn, "index.json", teams: teams)
  end

  def create(conn, %{"teams" => teams_params}) do
    jwt_users = Guardian.Plug.current_resource(conn)
    if (jwt_users.role == "General Manager" or jwt_users.role == "manager") do
      with {:ok, %Teams{} = teams} <- Data.create_teams(teams_params) do
        conn
        |> put_status(:created)
        |> put_resp_header("location", Routes.teams_path(conn, :show, teams))
        |> render("show.json", teams: teams)
      end
    else
      conn
      |> put_status(:unauthorized)
      |> render("401.json", message: "You cannot create a team because you are do not have sufficient admin rights. Ask your administrator for help.")
    end
  end

  def addUsersInTeams(conn, %{"users_teams" => teams_users_params}) do
    jwt_users = Guardian.Plug.current_resource(conn)
    if (jwt_users.role == "General Manager" or jwt_users.role == "manager") do
      IO.inspect(teams_users_params)
      with {:ok, %UsersTeams{} = users_teams} <- Data.add_users_teams(teams_users_params) do
      send_resp(conn, :created, "User succesfully added on team")
#      conn
#      |> put_status(:created)
#        #|> put_resp_header("location", Routes.teams_path(conn, :addUsersInTeams, users_teams))
#      |> redirect(to: Routes.teams_path(conn, :addUsersInTeams, teams_users_params))
      #|> render("addUsersInTeams.json", users_teams: teams_users_params)
      end
    else
      conn
      |> put_status(:unauthorized)
      |> render("401.json", message: "You cannot add a user on this team, you are not an manager or an general manager.")
    end
  end

  def getUsersInTeams(conn, %{"id" => id, "type" => type}) do
    jwt_users = Guardian.Plug.current_resource(conn)

    case type do
      "users" ->
        if (Integer.to_string(jwt_users.id) == id) do
          teams = Data.get_Teams_By_UserId(id)
          render(conn, "index_users_teams.json", teams: teams)
        else
          conn
          |> put_status(:unauthorized)
          |> render("401.json", message: "You can only retrieve the teams you are a member in.")
        end
      "teams" ->
        users = Data.get_Users_By_TeamId(id)
        render(conn, "index_teams_users.json", users: users)
    end
  end

  def deleteUsersInTeams(conn, %{"teamsId" => teamsId, "usersId" => usersId}) do
    jwt_users = Guardian.Plug.current_resource(conn)
    if (jwt_users.role == "General Manager" or jwt_users.role == "manager") do
      users = Data.get_one_users_teams!(teamsId, usersId)
      with {:ok, %UsersTeams{}} <- Data.delete_users_teams(users) do
        send_resp(conn, :no_content, "")
      end
    else
      conn
      |> put_status(:unauthorized)
      |> render("401.json", message: "You cannot delete a user on this team because you are not a manager or an general manager.")
    end
  end

  def show(conn, %{"id" => id}) do
    teams = Data.get_teams!(id)
    render(conn, "show.json", teams: teams)
  end

  def update(conn, %{"id" => id, "teams" => teams_params}) do
    jwt_users = Guardian.Plug.current_resource(conn)
    if (jwt_users.role == "General Manager" or jwt_users.role == "manager") do
      teams = Data.get_teams!(id)
      with {:ok, %Teams{} = teams} <- Data.update_teams(teams, teams_params) do
        render(conn, "show.json", teams: teams)
      end
    else
      conn
      |> put_status(:unauthorized)
      |> render("401.json", message: "You cannot update a team because you are not a manager or an general manager.")
    end
  end

  def delete(conn, %{"id" => id}) do
    jwt_users = Guardian.Plug.current_resource(conn)
    if (jwt_users.role == "General Manager" or jwt_users.role == "manager") do
      teams = Data.get_teams!(id)
      with {:ok, %Teams{}} <- Data.delete_teams(teams) do
        send_resp(conn, :no_content, "")
      end
    else
      conn
      |> put_status(:unauthorized)
      |> render("401.json", message: "You cannot remove a team because you are not a manager or a general manager.")
    end
  end
end
