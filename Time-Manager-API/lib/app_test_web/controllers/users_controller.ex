defmodule AppTestWeb.UsersController do
  use AppTestWeb, :controller

  alias AppTest.Data
  alias AppTest.Data.Users

  alias AppTest.Guardian

  action_fallback AppTestWeb.FallbackController

  def sign_out(conn, _params) do
    #TODO --> does not work currently!
#    jwt = get_req_header(conn, "authorization")
#    jwt = to_string(jwt)
#    jwt = String.replace(jwt, "Bearer ", "")
#    {:ok, claims} = AppTest.Guardian.revoke(jwt)
#    IO.inspect(claims)
    send_resp(conn, :no_content, "")
  end

  def sign_up(conn, %{"users" => users_params}) do
    with {:ok, %Users{} = users} <- Data.create_users(users_params),
         {:ok, token, _claims} <- Guardian.encode_and_sign(users) do
      conn
      |> put_status(:created)
      |> render("jwt.json", jwt: token)
    end
  end

  def sign_in(conn, %{"email" => email, "password" => password}) do
    case Data.token_sign_in(email, password) do
      {:ok, token, _claims} ->
        conn
        |> render("jwt.json", jwt: token)
      _ ->
        {:error, :unauthorized}
    end
  end

  def show(conn, %{"id" => id}) do
    users = Data.get_users!(id)
    render(conn, "users.json", users: users)
  end

  def get_all_users(conn, _params) do
    jwt_users = Guardian.Plug.current_resource(conn)
    if (jwt_users.role == "General Manager") do
      users = Data.list_users
      render(conn, "index.json", users: users)
    else
      conn
      |> put_status(:unauthorized)
      |> render("401.json", message: "Admin rights are not elevated enough. Contact your administrator for help.")
    end
  end

  def update(conn, %{"id" => id, "users" => users_params}) do
    jwt_users = Guardian.Plug.current_resource(conn)
    edited_users = Data.get_users!(id)
    if (jwt_users.role == "General Manager") do
      #the user is general manager he can change anything on anyone
      with {:ok, %Users{} = users} <- Data.update_users(edited_users, users_params) do
        render(conn, "users.json", users: users)
      end
    else
      #the user is lower than General Manager, and CANNOT modify roles
      if (jwt_users.id == edited_users.id) do
        #remove the role if the user tried, so that the other changes still take effect, but not the role change.
        if (Map.has_key?(users_params, "role")) do
          stripped_params = Map.delete(users_params, "role")
          with {:ok, %Users{} = users} <- Data.update_users(edited_users, stripped_params) do
            conn
            |> render("users.json", users: users)
          end
        else
          with {:ok, %Users{} = users} <- Data.update_users(edited_users, users_params) do
            conn
            |> render("users.json", users: users)
          end
        end
      else
        conn
        |> put_status(:unauthorized)
        |> render("401.json", message: "Your admin rights are not elevated enought to update other users.")
      end
    end
  end

  def delete(conn, %{"id" => id}) do
    #Only the General Manager can delete other users.
    #Managers and Employees can only delete their own user.
    jwt_users = Guardian.Plug.current_resource(conn)
    #jwt_users.id is an int while id is a string...
    if (jwt_users.role == "General Manager" or Integer.to_string(jwt_users.id) == id) do
      users = Data.get_users!(id)
      with {:ok, %Users{}} <- Data.delete_users(users) do
        send_resp(conn, :no_content, "")
      end
    else
      conn
      |> put_status(:unauthorized)
      |> render("401.json", message: "Your admin rights are not elevated enought to delete other users.")
    end
  end

end