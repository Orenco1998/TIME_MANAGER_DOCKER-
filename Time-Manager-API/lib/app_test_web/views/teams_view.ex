defmodule AppTestWeb.TeamsView do
  use AppTestWeb, :view
  alias AppTestWeb.TeamsView
  alias AppTestWeb.UsersView

  def render("index.json", %{teams: teams}) do
    %{data: render_many(teams, TeamsView, "teams.json")}
  end

  def render("show.json", %{teams: teams}) do
    %{data: render_one(teams, TeamsView, "teams.json")}
  end

  def render("teams.json", %{teams: teams}) do
    %{
      id: teams.id,
      name: teams.name
    }
  end

  def render("users_teams.json", %{"teams_id" => teams_id, "users_id" => users_id}) do
    #TODO fix
    %{
      teams_id: teams_id,
      users_id: users_id
    }
  end

  def render("addUsersInTeams.json", %{"teams_id" => teams_id, "users_id" => users_id}) do
    %{data: render_one(teams_id, users_id, TeamsView, "users_teams.json")}
  end

  def render("index_users_teams.json", %{teams: teams}) do
    %{data: render_many(teams, TeamsView, "teams.json")}
  end

  def render("index_teams_users.json", %{users: users}) do
    %{data: render_many(users, UsersView, "users.json")}
  end

  def render("401.json", %{message: message}) do
    %{
      errors: %{
        detail: message
      }
    }
  end
end
