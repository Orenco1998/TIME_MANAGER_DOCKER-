defmodule AppTestWeb.UsersView do
  use AppTestWeb, :view
  alias AppTestWeb.UsersView

  def render("index.json", %{users: users}) do
    %{data: render_many(users, UsersView, "users.json")}
  end

  def render("users.json", %{users: users}) do
    %{
      id: users.id,
      username: users.username,
      email: users.email,
      role: users.role,
    }
  end

  def render("sign_in.json", %{users: users, token: token}) do
    %{
      id: users.id,
      username: users.username,
      email: users.email,
      role: users.role,
      token: token
      #password: users.password
    }
  end

  def render("jwt.json", %{jwt: jwt}) do
    %{token: jwt}
  end

  def render("401.json", %{message: message}) do
    %{
      errors: %{
        detail: message
      }
    }
  end
end
